a.) Nodos: 599207
b.) Altura: 26
c.) Promedio: 18.0
d.) Altura te�rica m�xima red-black: 2 veces la altura de la rama izquierda
e.) Altura te�rica m�nima red-black: Cuando la altura es igual por izquierda que por derecha
f.) Altura te�rica m�xima 2-3: 1 + altura por izquierda
g.) Altura te�rica m�nima 2-3: La misma altura por ambas ramas
h.) Aparentemente el �rbol real tiene la m�xima altura posible ya que la altura es 26 y el promedio de
    la altura que tuvo que recorrer fue la mitad (18). A comparaci�n del �rbol 2-3, la altura es mucho
    mayor en el Red-Black dado que la m�xima diferencia de altura entre las ramas (izquierda - derecha)
    es de 1
i.) El promedio de las alturas de los nodos es igual al del �rbol red-black cuando tiene su m�xima 
    altura. Cuando el �rbol red-black tiene su altura m�nima, el promedio de de las alturas 
    de cada nodo deber�a ser menor al promedio de de las alturas del �rbol real. El promedio en los
    �rboles 2-3 deber�a ser menor al promedio real del �rbol red-black ya que en los �rboles 2-3
    no existe una diferencia tan grande de altura entre ambas ramas del �rbol.