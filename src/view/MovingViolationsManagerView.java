package view;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() 
	{

	}

	public void printMenu() 
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 7----------------------");
		//LOAD
		System.out.println("1. Cargar datos");
		//LOAD
		System.out.println("2. Dar información de infracción por ObjectID");
		System.out.println("3. Dar información de las infracciones en un rango dado");
		//EXIT
		System.out.println("0. Salir");
		//EXIT
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}



	public void printMovingViolations(IQueue<VOMovingViolations> violations) 
	{
		System.out.println("Se encontraron "+ violations.size() + " elementos");
		for (VOMovingViolations violation : violations) 
		{
			System.out.println(violation.getObjectId() + " " 
					+ violation.getTicketIssueDate() + " " 
					+ violation.getLocation()+ " " 
					+ violation.getViolationDescription());
		}
	}

	public void printMensage(String mensaje) 
	{
		System.out.println(mensaje);
	}
}
