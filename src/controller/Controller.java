package controller;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.Stack;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> QueueCopy;

	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;

	private final static String[] mths = {"January", "February", "March", "April", "May", "June"};

	/**
	 * �rbol balancedo donde se van a cargar los datos de los archivos
	 */
	private RedBlackBST<Integer, VOMovingViolations> bst;

	public Controller() {
		view = new MovingViolationsManagerView();

		// TODO, inicializar la pila y la cola
		movingViolationsQueue = new Queue<VOMovingViolations>();
		movingViolationsStack = new Stack<VOMovingViolations>();
		bst = new RedBlackBST<Integer, VOMovingViolations>();
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean loaded = false;
		boolean fin = false;

		while (!fin) {
			view.printMenu();

			int option = sc.nextInt();

			switch (option) {
			case 1: // CARGAR
				if (!loaded) 
				{
					long a = System.currentTimeMillis();
					this.ReadJSONExample();
					long b = System.currentTimeMillis();
					System.out.println("Tiempo de carga: " + (b-a));
					loaded = true;
					break;
				} 

				else 
				{
					System.out.println();
					System.out.println("Los datos ya fueron cargados (Solo se pueden cargar una vez)");
					System.out.println();
					break;
				}

			case 2:
				if (loaded)
				{
					System.out.println("Introduzca el id de la infracci�n");
					int id = sc.nextInt();
					consultarInfraccion(id);
				}
				else
				{
					System.out.println("Los datos deben ser cargados primero");
				}
				break;
				
			case 3:
				if (loaded)
				{
					System.out.println("Introduzca el id de la infracci�n menor");
					int min = sc.nextInt();
					System.out.println("Introduzca el id de la infracci�n mayor");
					int max = sc.nextInt();
					if (min > max)
					{
						System.out.println("El id menor no puede ser mayor que el id mayor"); break;
					}
					consultarRango(min, max);
					break;
				}
				else
				{
					System.out.println("Los datos deben ser cargados primero");
				}
				break;
				
			case 4:
				if (loaded)
					System.out.println("Altura: " + bst.height());
				break;
			case 5:
				if (loaded)
				{
					int sum = 0;
					int total = 0;
					Iterator<Integer> it = bst.keys().iterator();
					while (it.hasNext())
					{
						int key = it.next();
						int height = bst.getHeight(key);
						sum += height;
						total++;
					}
					double promedio = sum/total;
					System.out.println("Promedio: "+promedio);
				}
				break;
				
			case 0:
				fin = true;
				sc.close();
				break;
			}
		}
	}

	public void ReadJSONExample() 
	{
		int total = 0;
		for (int i = 0; i < mths.length; i++) 
		{
			int tempNum = 0;
			String path = "./data/Moving_Violations_Issued_in_"+mths[i]+"_2018.json";
			try
			{
				FileReader fr = new FileReader(path);
				JsonReader jr = new JsonReader(fr);
				jr.beginArray();
				Gson gson = new GsonBuilder().create();
				
				while (jr.hasNext())
				{
					VOMovingViolations vomv = gson.fromJson(jr, VOMovingViolations.class);
					bst.put(vomv.getObjectId(), vomv);
					tempNum++;
				}
				fr.close();
				jr.close();
			}
			catch (IOException e)
			{
				System.out.println("No se pudo cargar la informaci�n del archivo");
				e.printStackTrace();
			}
			total += tempNum;
			System.out.println("En el mes de " + mths[i] + " se encontraron " + tempNum + " infracciones");
		}
		System.out.println("Se encontraron " + total + " infracciones en total");
		System.out.println();
		System.out.println("�Los datos fueron cargados!");
		System.out.println();
	}
	
	
	public void consultarInfraccion(int id)
	{
		if (bst.contains(id))
		{
			VOMovingViolations vomv = bst.get(id);
			System.out.println( "LOCATION: "+vomv.getLocation()+"; "+
								"ADDRESS: "+vomv.getAdress()+"; "+
								"STREETSEGID: "+vomv.getStreetSegId()+"; "+
								"XCORD: "+vomv.getXCord()+"; "+
								"YCORD: "+vomv.getYCord()+"; "+
								"TICKETISSUEDATE: "+vomv.getTicketIssueDate() );
		}
		else
		{
			System.out.println("El id introducido no se encontr�");
		}
	}
	
	public void consultarRango(int min, int max)
	{
		Iterator<VOMovingViolations> it = bst.valuesInRange(min, max);
		while (it.hasNext())
		{
			VOMovingViolations vomv = it.next();
			System.out.println( "LOCATION: "+vomv.getLocation()+"; "+
					"ADDRESS: "+vomv.getAdress()+"; "+
					"STREETSEGID: "+vomv.getStreetSegId()+"; "+
					"XCORD: "+vomv.getXCord()+"; "+
					"YCORD: "+vomv.getYCord()+"; "+
					"TICKETISSUEDATE: "+vomv.getTicketIssueDate() );
		}
	}
}
