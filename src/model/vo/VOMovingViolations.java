package model.vo;
import java.text.SimpleDateFormat;

import java.util.Date;

import com.google.gson.annotations.SerializedName;


/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>
{
	@SerializedName("OBJECTID")
	private int objectID;

	@SerializedName("LOCATION")
	private String location;

	@SerializedName("ADDRESS_ID")
	private int adressID;
	
	@SerializedName("STREETSEGID")
	private double streetSegID;

	@SerializedName("XCOORD")
	private double xCord;

	@SerializedName("YCOORD")
	private double yCord;

	@SerializedName("TICKETTYPE")
	private String ticketType;

	@SerializedName("FINEAMT")
	private int fineamt;

	@SerializedName("TOTALPAID")
	private double totalPaid;

	@SerializedName("PENALTY1")
	private int penalty1;

	@SerializedName("ACCIDENTINDICATOR")
	private String accidentIndicator;

	@SerializedName("TICKETISSUEDATE")
	private String ticketIssueDate;

	@SerializedName("VIOLATIONCODE")
	private String violationCode;

	@SerializedName("VIOLATIONDESC")
	private String violationDesc;

	public VOMovingViolations(int objectId, String location, int adress, double streetSegId, double xCord, double yCord,
			String ticketType, int fineamt, double totalPaid, int penalty1, String accidentIndicator,
			String ticketIssueDate, String violationCode, String violationDesc) 
	{
		this.objectID = objectId;
		this.location = location;
		this.adressID = adress;
		this.streetSegID = streetSegId;
		this.xCord = xCord;
		this.yCord = yCord;
		this.ticketType = ticketType;
		this.fineamt = fineamt;
		this.totalPaid = totalPaid;
		this.penalty1 = penalty1;
		this.accidentIndicator = accidentIndicator;
		this.ticketIssueDate = ticketIssueDate;
		this.violationCode = violationCode;
		this.violationDesc = violationDesc;
	}


	/**
	 * @return id - Identificador �nico de la infracci�n
	 */
	public int getObjectId() {
		// TODO Auto-generated method stub
		return objectID;
	}	


	/**
	 * @return location - Direcci�n en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracci�n .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag� el que recibi� la infracci�n en USD.
	 */
	public double getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}

	/**
	 * @return description - Descripci�n textual de la infracci�n.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDesc;
	}
	
	public int getFineAmt()
	{
		return fineamt;
	}
	
	public String getViolationCode() {
		return violationCode;
	}
	
	public int getPenalty1() {
		return penalty1;
	}
	
	public Date getDate() throws Exception {
		try {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		Date d1 = sdf.parse(ticketIssueDate);
		return d1;
		}
		catch (Exception e){
			e.printStackTrace();
			throw e;
			}
		}

	public int getAdress(){
		return adressID;
		}
	
	public double getStreetSegId()
	{
		return streetSegID;
	}
	
	public double getXCord()
	{
		return xCord;
	}
	
	public double getYCord()
	{
		return yCord;
	}
	
	public String getHour() {
		return ticketIssueDate.substring(11);
	}
	
	public String toString()
	{
		return objectID + "; " + location + "; " + ticketIssueDate + "; " + violationCode + "; " + fineamt;
	}
	
	@Override

	public int compareTo(VOMovingViolations that) {	
	
		return this.ticketIssueDate.compareTo(that.ticketIssueDate);
	}
	
}
