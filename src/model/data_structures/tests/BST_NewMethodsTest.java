package model.data_structures.tests;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.Assert;

import model.data_structures.RedBlackBST;

public class BST_NewMethodsTest 
{
	@Test
	public void testSize()
	{
		RedBlackBST<Integer, Integer> bst = new RedBlackBST<Integer, Integer>();
		assert(bst.size()==0);
		bst.put(1, 1);
		assert(bst.size()==1);
		for (int i = 2; i <= 10; i++)
		{
			bst.put(i, i);
		}
		assert(bst.size()==10);
		bst.put(10, 10);
		assert(bst.size()==10);
	}

	@Test
	public void testEmpty()
	{
		RedBlackBST<Integer, Integer> bst = new RedBlackBST<Integer, Integer>();
		assert(bst.isEmpty());
	}

	@Test
	public void testGet()
	{
		RedBlackBST<Integer, Integer> bst = new RedBlackBST<Integer, Integer>();
		try
		{
			bst.get(null);
			fail();
		}
		catch (Exception e){ 
			//Debe entrar aqu�
		}
		assert(bst.get(1)==null);
		bst.put(1, 1);
		assert(bst.get(1)==1);
		for (int i = 2; i <= 10; i++)
		{
			bst.put(i, i);
		}
		assert(bst.get(10)==10);
		assert(bst.get(5)==5);
		for (int i = -10; i <= 0; i++)
		{
			bst.put(i, Math.abs(i));
		}
		assert(bst.get(-10)==10);
		assert(bst.get(-5)==5);
	}

	public void testGetHeight()
	{
		RedBlackBST<Integer, Integer> bst = new RedBlackBST<Integer, Integer>();
		assert(bst.getHeight(2)==-1);
		bst.put(0, 1);
		assert(bst.getHeight(0)==0);
		bst = new RedBlackBST<Integer, Integer>();
		for (int i = -10; i <= 0; i++)
		{
			bst.put(i, Math.abs(i));
		}
		assert(bst.getHeight(0) <= bst.size()/2 && bst.getHeight(0) > bst.size()/4);
	}

	public void testContains()
	{
		RedBlackBST<Integer, Integer> bst = new RedBlackBST<Integer, Integer>();
		assert(bst.contains(0)==false);
		for (int i = -10; i <= 0; i++)
		{
			bst.put(i, Math.abs(i));
		}

		for (int i = -10; i <= 0; i++)
		{
			assert(bst.contains(i));
		}
	}

	public void testPut()
	{
		RedBlackBST<Integer, Integer> bst = new RedBlackBST<Integer, Integer>();
		try
		{
			bst.put(null, null);
			fail();
		}
		catch (Exception e)
		{
			//Deber�a entrar aqu�
		}
		bst.put(1, 1);
		assert(bst.contains(1));
		for (int i = -10; i <= 0; i++)
		{
			bst.put(i, Math.abs(i));
		}
		for (int i = -10; i <= 0; i++)
		{
			assert(bst.contains(i));
		}
	}
	
	public void testHeight()
	{
		RedBlackBST<Integer, Integer> bst = new RedBlackBST<Integer, Integer>();
		assert(bst.height() == 0);
		for (int i = -10; i <= 0; i++)
		{
			bst.put(i, Math.abs(i));
		}
		assert (bst.getHeight(0) <= bst.size()/2 && bst.getHeight(0) > bst.size()/4);
	}
	
	public void testMax()
	{
		RedBlackBST<Integer, Integer> bst = new RedBlackBST<Integer, Integer>();
		try
		{
			bst.max();
			fail();
		}
		catch (Exception e){
			//Deber�a entrar aqu�
		}
		for (int i = -10; i <= 0; i++)
		{
			bst.put(i, Math.abs(i));
		}
		assert(bst.max() == 10);
	}
	
	public void testMin()
	{
		RedBlackBST<Integer, Integer> bst = new RedBlackBST<Integer, Integer>();
		try
		{
			bst.max();
			fail();
		}
		catch (Exception e){
			//Deber�a entrar aqu�
		}
		for (int i = -10; i <= 0; i++)
		{
			bst.put(i, Math.abs(i));
		}
		assert(bst.max() == 0);
	}
}

